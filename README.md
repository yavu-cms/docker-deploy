# Yavu deployment files

Este repositorio sirve de ejemplo de como iniciar el stack de yavu usando
docker-compose. Idealmente este proyecto debería correrse en un scheduller de
docker del estilo de:

* [Rancher Cattle](https://rancher.com/docs/rancher/v1.6/en/)
* [Docker Swarm](https://docs.docker.com/engine/swarm/)
* [Kubernetes](https://kubernetes.io/)


## Uso con docker-compose

Si no se utiliza un scheduller, puede usarse docker únicamente con
[docker compose](https://docs.docker.com/compose/), y no tener alta
disponibilidad, algo que para ambientes pequeños es más que suficiente.
Este repositorio tiene la configuración propia de un yavu que correrá en un
equipo solamente, teniendo como único requerimiento la instalación de docker y
docker compose.
Para la solución basada en un scheduller, debe referirse a la tecnología de
clustering seleccionada y proceder de forma individual para configurar su
ambiente.

### Prerequisitos

Clonar este repositorio y continuar configurando el ambiente.

Para simplificar la configuración del ambiente de despliegue, se utilizan
variables de ambiente dentro del `docker-compose.yml`. Dichas variables pueden
verse analizando el archivo `.envrc-sample`.
Para simplificar el trabajo con variables de ambiente se propone utilizar
[direnv](https://direnv.net/) y copiar el archivo `.envrc-sample` en `.envrc`.
Luego editar dicho archivo con los datos propios del sitio que vaya a hostear. A
modo de ejemplo se provee una configuración con tres dominios, configurados de
la siguiente forma:

  * **VIRTUALHOST:** www.example.com, es el nombre del portal público
  * **WSOCK_HOSTNAME:** ws.example.com, es el hostname usada por los websockets que
    se utilizan para el backend de administración
  * **WSOCK_URL:**  en esta URL es importante el protocolo. Si el backend
    funcionará con SSL, entonces esta URL debería ser también por SSL
  * **BACKEND_HOSTNAME:** admin.example.com, es la URL del backend de
    administración

Luego se definen variables para configurar el mailer que permitirá la
notificación de errores al webmaster ante posibles problemas en el código del
producto.

> Las pruebas locales pueden hacerse con los dominios propuestos, para lo cuál
> es necesario agregar en el archivo `/etc/hosts` de la máquina del
> desarrollador la siguiente entrada:
> `127.0.10.10 www.example.com ws.example.com admin.example.com`

### Usar SSL

En caso de querer usar SSL, es necesario agregar un servicio más al stack
provisto. Para más información leer
[aquí](https://github.com/JrCs/docker-letsencrypt-nginx-proxy-companion)

### Iniciando el stack

```
docker-compose up -d
```

El comando anterior iniciará el stack completo, los logs podrán verse con el
comando:

```
docker-compose logs -f
```

En caso de querer ver el estado de los contenedores del stack completo:

```
docker-compose ps
```

Para conectar con algún contenedor:

```
docker-compose exec <CONTAINER_NAME> CMD
```

### Datos iniciales

La primera vez que se inicia yavu, si detecta que no hay parches que aplicar en
la base de datos, e incluso si detecta que no haya datos, se inicializa todo el
contexto funcional del producto para su acceso por primera vez.

Los datos de acceso son:

* **Usuario:** admin
* **Password:** yavu!

Dichos datos se solicitan para acceder al portal de administración configurado
en la URL seteada en `.envrc`.

### Hacer un dump del mysql

Para hacer un dump del mysql actual:

```
docker exec -i $(docker-compose ps | grep db | awk '{print $1}')\
  mysqldump yavu -proot \
  --single-transaction \
  --flush-logs \
  --master-data=2 \
  --quick | gzip > /tmp/yavu.sql.gz
```

### Backups de los datos

En caso de querer realizar un backups de los volúmenes de yavu, los mismos se
corresponden con el nombre del proyecto de docker-compose (el directorio actual)
seguido por el nombre del volumen declarado. Dichos archivos se encuentran en el
directorio `/var/lib/docker/volumes`

### ¿Como cambio algún valor del docker compose?

Sea porque el docker-compose es editado o alguna variable es redefinida, es
posible que los cambios se apliquen usando:

```
docker-compose up -d --force-recreate <CONTENEDOR>
```

Si no se especifica un contenedor, todo el stack es recreado y los cambios
aplicados.

## ¿No funciona el frontend?

Yavu es un backend para N sitios. El actual docker-compose provee un único
frontend, el cuál se configura con la variable VIRTUAL_HOST.
sin embargo, cuando se incia por primera vez el stack, el backend debe indicar
que el frontend atenderá bajo la URL configurada.
Esta configuración se realiza accediendo al backend de administración y luego al
menú: 

* **Administración**
  * **Servidores del frontend:** en este módulo debe seleccionarse el frontend
    deseado (que en una configuración inicial será uno sólo), y se le puede
    asignar una aplicación cliente

> Puede que el servicio de caching por delante del frontend (varnish) no
> refresque rápidamente. Este problema puede solucionarse manualmente con el
> comando `docker-compose up --force-recreate -d varnish`
